package com.cheata.ass1.data.api.service;

import com.cheata.ass1.data.api.entities.Article;
import com.cheata.ass1.data.api.response.ArticleResponse1;
import com.cheata.ass1.data.api.response.ArticlesResponse;
import com.cheata.ass1.data.api.response.ImageResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {

    @Multipart
    @POST("v1/api/uploadfile/single")
    Call<ImageResponse> uploadImage(@Part MultipartBody.Part img);

    @GET("v1/api/articles")
    Call<ArticlesResponse> findAll(@Query("page")int page);

    @GET("v1/api/articles/{id}")
    Call<ArticleResponse1> findOne(@Path("id")int id);

    @DELETE("v1/api/articles/{id}")
    Call<Article> deleteOne(@Path("id")int id);

    @PUT("v1/api/articles/{id}")
    Call<Article> update(@Path("id")int id,@Body Article article);

    @POST("v1/api/articles")
    Call<Article> addArticle(@Body Article article);
}
