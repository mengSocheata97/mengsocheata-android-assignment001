package com.cheata.ass1.data.api.response;

import com.cheata.ass1.data.api.entities.Article;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleResponse1 {

    @SerializedName("DATA")
    private Article data;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("CODE")
    private String code;

    public ArticleResponse1(Article data, String message, String code) {
        this.data = data;
        this.message = message;
        this.code = code;
    }
    public Article getData() {
        return data;
    }

    public void setData(Article data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
