package com.cheata.ass1.ui.addactivity.mvp;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.cheata.ass1.data.api.ArticleServiceConverter;
import com.cheata.ass1.data.api.entities.Article;
import com.cheata.ass1.data.api.response.ArticleResponse1;
import com.cheata.ass1.data.api.response.ImageResponse;
import com.cheata.ass1.data.api.service.ArticleService;
import com.cheata.ass1.ulities.UtilitiesFile;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AddInteractor implements AddMVP.AddInteractor {

    private ArticleService articleService = ArticleServiceConverter.createService(ArticleService.class);

    @Override
    public void onUpdateArticle(int id, Article article, final UpdateInteractorCallback callback) {
        Call<Article> articleCall = articleService.update(id,article);
        articleCall.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(Call<Article> call, Response<Article> response) {
                callback.onUpdateSuccess();
            }

            @Override
            public void onFailure(Call<Article> call, Throwable t) {
                callback.onUpdateFailed(t);
            }
        });
    }

    @Override
    public void onGetArticleById(int id, final GetArticleByIdInteractorCallback callback) {
        Call<ArticleResponse1> articleCall = articleService.findOne(id);
        articleCall.enqueue(new Callback<ArticleResponse1>() {
            @Override
            public void onResponse(Call<ArticleResponse1> call, Response<ArticleResponse1> response) {
                if(response.body()!=null){
                    callback.onGetArticleByIdSuccess(response.body());
                    Log.i("ss", "onResponse: "+response.body().getData().getImage());
                }

            }

            @Override
            public void onFailure(Call<ArticleResponse1> call, Throwable t) {
                callback.onGetArticleByIdFailed(t);
            }
        });

    }

    @Override
    public void onAddArticle(Article article, final AddInteractorCallback callback) {
        Call<Article> articleCall = articleService.addArticle(article);
        articleCall.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(Call<Article> call, Response<Article> response) {
                callback.onAddSuccess();
                Log.i("add", "onResponse: added"+response.body().getTitle());
            }
            @Override
            public void onFailure(Call<Article> call, Throwable t) {
                callback.onAddFailed(t);
            }
        });
    }

    @Override
    public void uploadImg(Uri uri, Context context, final UploadImageInteractorCallback callback) {
        File f = new File(UtilitiesFile.getPath(uri, context));
        final RequestBody requestBody = RequestBody.create(MediaType.parse("form-data"),f);
        MultipartBody.Part part = MultipartBody.Part.createFormData("FILE",f.getName(),requestBody);

        Call<ImageResponse> imageResponseCall = articleService.uploadImage(part);
        imageResponseCall.enqueue(new Callback<ImageResponse>() {
            @Override
            public void onResponse(Call<ImageResponse> call, Response<ImageResponse> response) {
                callback.onUploadSuccess(response.body().getData());
                Log.d(TAG, "onResponse: "+response.body().getData());
            }

            @Override
            public void onFailure(Call<ImageResponse> call, Throwable t) {
                callback.onUploadFailed(t);
            }
        });
    }
}