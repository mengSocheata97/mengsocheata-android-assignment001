package com.cheata.ass1.ui.addactivity.mvp;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;

import com.cheata.ass1.data.api.entities.Article;
import com.cheata.ass1.data.api.response.ArticleResponse1;

import java.util.List;

public interface AddMVP {

    interface AddView{
        void onPermissionsGranted(int requestCode, @NonNull List<String> perms);

        void onDestroyView();
        void onFailedView(Throwable t);
        void onGetSuccess(Article article);
        void onAddImageSuccess(String path);
        void onPermissionsDenied(int requestCode, @NonNull List<String> perms);
    }
    interface AddPresenter{

        void addArticle(Article article);
        void getArticleById(int id);
        void addImageToServer(Context context, Uri img);
        void updateArticleById(int id,Article article);

    }
    interface AddInteractor{

        void onUpdateArticle(int id,Article article,UpdateInteractorCallback callback);
        interface UpdateInteractorCallback{
            void onUpdateSuccess();
            void onUpdateFailed(Throwable t);
        }

        void onGetArticleById(int id,GetArticleByIdInteractorCallback callback);
        interface GetArticleByIdInteractorCallback{
            void onGetArticleByIdSuccess(ArticleResponse1 article);
            void onGetArticleByIdFailed(Throwable t);
        }

        void onAddArticle(Article article,AddInteractorCallback callback);
        interface AddInteractorCallback{
            void onAddSuccess();
            void onAddFailed(Throwable t);
        }

        void uploadImg(Uri uri, Context context,UploadImageInteractorCallback callback );
        interface UploadImageInteractorCallback{
            void onUploadSuccess(String path);
            void onUploadFailed(Throwable t);
        }
    }
}
