package com.cheata.ass1.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.cheata.ass1.R;
import com.cheata.ass1.adapter.ArticleAdapter;
import com.cheata.ass1.adapter.LoadMoreAdapter;
import com.cheata.ass1.data.api.entities.Article;
import com.cheata.ass1.ui.addactivity.AddArticle;
import com.cheata.ass1.ui.main.mvp.ArticleMVP;
import com.cheata.ass1.ui.main.mvp.ArticlePresenter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ArticleMVP.View {

    private List<Article> articles = new ArrayList<>();
    private RecyclerView recyclerView;
    private ArticleAdapter articleAdapter;
    private FloatingActionButton floatingActionButton;
    private SwipeRefreshLayout refreshLayout;
    private ArticleMVP.Presenter presenter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layoutManager = new LinearLayoutManager(this);

        initialized();

        presenter = new ArticlePresenter(this);
        presenter.setView(this);
        presenter.onFindAllArticles();

        recyclerView.setLayoutManager(layoutManager);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddArticle.class);
                intent.putExtra("editView",2);
                startActivity(intent);
            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                articles.clear();
               presenter.onFindAllArticles();
               refreshLayout.setRefreshing(false);
            }
        });
    }

    public void initialized()
    {
        floatingActionButton = findViewById(R.id.my_fl_button);
        recyclerView = findViewById(R.id.my_recycler);
        refreshLayout = findViewById(R.id.swipe_container);

        recyclerView.addOnScrollListener(new LoadMoreAdapter(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemCount, RecyclerView view) {
                presenter.onLoadMore(page);
            }
        });
    }

    @Override
    public void onReloadData(List<Article> articleList) {
        articles.addAll(articleList);
        articleAdapter.notifyDataSetChanged();
    }

    @Override
    public void onArticleResponse(List<Article> articleList) {
        articles.addAll(articleList);
        articleAdapter = new ArticleAdapter(articles,this,presenter);
        recyclerView.setAdapter(articleAdapter);
        articleAdapter.notifyDataSetChanged();
        Toast.makeText(MainActivity.this,"hello",Toast.LENGTH_SHORT).show();
        Log.e("" +
                "" +
                "hello", "onArticleResponse: " + articleList.toString() );
    }

    @Override
    public void onArticleResponeFail(String message) {
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        articles.clear();
        presenter.onFindAllArticles();
        refreshLayout.setRefreshing(false);
    }
}
