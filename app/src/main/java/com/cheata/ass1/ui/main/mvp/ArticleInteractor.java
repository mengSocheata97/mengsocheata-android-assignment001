package com.cheata.ass1.ui.main.mvp;

import android.util.Log;

import com.cheata.ass1.data.api.ArticleServiceConverter;
import com.cheata.ass1.data.api.entities.Article;
import com.cheata.ass1.data.api.response.ArticlesResponse;
import com.cheata.ass1.data.api.service.ArticleService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArticleInteractor implements ArticleMVP.Interactor {

    private ArticleService articleService;
    private Article article;

    public ArticleInteractor() {

        articleService = ArticleServiceConverter.createService(ArticleService.class);
    }
    @Override
    public void findAllArticles(int page , final InteractorCallBack callback) {
        final Call<ArticlesResponse> responseCall = articleService.findAll(page);
        responseCall.enqueue(new Callback<ArticlesResponse>() {
            @Override
            public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                Log.i("dd", "onResponse: "+response.body());
                List<Article> articles = response.body().getData();
                callback.onSuccess(articles);
            }
            @Override
            public void onFailure(Call<ArticlesResponse> call, Throwable t) {
                callback.onFailed();
                Log.d("TAG", "onFailure: "+t.toString());
            }
        });
    }

    @Override
    public void deleteArticleById(int id, final DeleteInterractorCallback callBack) {
        Call<Article> articleCall = articleService.deleteOne(id);
        articleCall.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(Call<Article> call, Response<Article> response) {
                callBack.onDeleteSuccess();
            }

            @Override
            public void onFailure(Call<Article> call, Throwable t) {
                callBack.onDeleteFailed(t);
            }
        });
    }

    @Override
    public void loadMorePage(int page, final LoadMorePageInteractorCallback callback) {
        Call<ArticlesResponse> call = articleService.findAll(page);
        call.enqueue(new Callback<ArticlesResponse>() {
            @Override
            public void onResponse(Call<ArticlesResponse> call, Response<ArticlesResponse> response) {
                callback.onLoadMoreSuccess(response.body().getData());
            }

            @Override
            public void onFailure(Call<ArticlesResponse> call, Throwable t) {
                callback.onLoadMoreFail(t);
            }
        });
    }
}
