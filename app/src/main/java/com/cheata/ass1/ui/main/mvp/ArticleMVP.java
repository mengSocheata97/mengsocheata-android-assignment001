package com.cheata.ass1.ui.main.mvp;

import com.cheata.ass1.data.api.entities.Article;

import java.util.List;

public interface ArticleMVP {

    interface View{
        void onReloadData(List<Article> articleList);
        void onArticleResponse(List<Article> articleList);
        void onArticleResponeFail(String message);
    }
    interface Presenter{
        void onFindAllArticles();
        void onDeleteArticleById(int id);
        void setView(View view);
        void onLoadMore(int page);
    }
    interface Interactor{

        void findAllArticles(int page,InteractorCallBack callback);
        void deleteArticleById(int id, DeleteInterractorCallback callBack);
        void loadMorePage(int page, LoadMorePageInteractorCallback callback);

        interface LoadMorePageInteractorCallback{
            void onLoadMoreSuccess(List<Article> list);
            void onLoadMoreFail(Throwable t);
        }

        interface DeleteInterractorCallback{
            void onDeleteSuccess();
            void onDeleteFailed(Throwable t);
        }
        interface InteractorCallBack
        {
            void onSuccess(List<Article> articles);
            void onFailed();
        }
    }
}
