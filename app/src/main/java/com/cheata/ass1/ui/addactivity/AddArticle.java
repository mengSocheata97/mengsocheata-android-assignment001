package com.cheata.ass1.ui.addactivity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cheata.ass1.R;
import com.cheata.ass1.data.api.entities.Article;
import com.cheata.ass1.ui.addactivity.mvp.AddMVP;
import com.cheata.ass1.ui.addactivity.mvp.AddPresenter;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class AddArticle extends AppCompatActivity implements AddMVP.AddView {

    private static final int GALLERY_PERMISSION_CODE = 1001;
    private Button btnSave, btnCancel;
    private TextView etextTitle, etextDesc;
    private ImageView eImgPro;
    private ProgressBar addProgressBar;
    private AddPresenter presenter;
    private Article article;
    private final int GALLERY_REQUEST_CODE = 1000;
    private Uri uri;
    private String path = null;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_article);

        id = getIntent().getIntExtra("articleID",0);
        final int ui = getIntent().getIntExtra("editView", 1);
        Log.i("get", "onCreate: "+ ui);

        initialized();

        eImgPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImage();
            }
        });

        if(ui ==1)
        {
            presenter.getArticleById(id);
            btnSave.setText("Update");

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addProgressBar.setVisibility(View.VISIBLE);
                    if(uri==null)
                        onUpdate(id,path);
                    else
                        presenter.addImageToServer(getApplicationContext(),uri);

                }
            });
        }else{
            btnSave.setText("Save");
            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addProgressBar.setVisibility(View.VISIBLE);
                    if(uri==null){
                        onAdd(null);
                    }else{
                        presenter.addImageToServer(getApplicationContext(),uri);
                    }
                }
            });
        }
    }

    @AfterPermissionGranted(GALLERY_PERMISSION_CODE)
    private void openImage() {
        String[] perms = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };
        if(EasyPermissions.hasPermissions(this,perms)){
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent,GALLERY_REQUEST_CODE);
        }else{
            EasyPermissions.requestPermissions(this,"allow to pick image",GALLERY_REQUEST_CODE,perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        EasyPermissions.requestPermissions(this,"Please Allow to pick image",GALLERY_REQUEST_CODE);
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
        }
    }

    private void onAdd(String img){
        presenter.addArticle(new Article(
                img,
                System.currentTimeMillis()/1000+"",
                etextDesc.getText().toString(),
                etextTitle.getText().toString()
        ));
    }

    private void onUpdate(int id,String img){
        presenter.updateArticleById(id,new Article(
                img,
                System.currentTimeMillis()/1000+"",
                etextDesc.getText().toString(),
                etextTitle.getText().toString()
        ));
    }

    public void initialized(){
        presenter = new AddPresenter(this);

        btnSave = findViewById(R.id.edit_btn_save);
        btnCancel = findViewById(R.id.edit_btn_cancel);
        etextDesc = findViewById(R.id.add_desc);
        etextTitle = findViewById(R.id.add_title);
        eImgPro = findViewById(R.id.add_img);
        addProgressBar = findViewById(R.id.add_progressBar);
        addProgressBar.setVisibility(View.GONE);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onDestroyView() {
        finish();
    }

    @Override
    public void onFailedView(Throwable t) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false)
                .setTitle("Error")
                .setMessage("Error: "+t.getMessage());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
    }

    @Override
    public void onGetSuccess(Article article) {
        if(article!=null){
            this.article = article;
            Glide.with(this).load(article.getImage())
                    .placeholder(R.drawable.blue_dafault)
                    .into(eImgPro);
            etextTitle.setText(article.getTitle());
            etextDesc.setText(article.getDescription());
            etextTitle.setText(article.getTitle());
            etextDesc.setText(article.getDescription());
            this.path = article.getImage();
        }
    }

    @Override
    public void onAddImageSuccess(String path) {
        if(btnSave.getText().equals("Save")){
            onAdd(path);
        }else{
            onUpdate(id,path);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==GALLERY_REQUEST_CODE){
            assert data != null;
            uri = data.getData();
            Glide.with(getApplicationContext())
                    .load(uri)
                    .into(eImgPro);
        }
    }

}
