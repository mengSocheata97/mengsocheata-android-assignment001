package com.cheata.ass1.ui.main.mvp;

import com.cheata.ass1.data.api.entities.Article;

import java.util.List;

public class ArticlePresenter implements ArticleMVP.Presenter {

    private ArticleMVP.View view;
    private ArticleMVP.Interactor interactor;

    public ArticlePresenter(ArticleMVP.View view){
        this.view = view;
        interactor  = new ArticleInteractor();
    }

    @Override
    public void onFindAllArticles() {
        interactor.findAllArticles(1, new ArticleMVP.Interactor.InteractorCallBack() {
            @Override
            public void onSuccess(List<Article> articles) {
                view.onArticleResponse(articles);
            }
            @Override
            public void onFailed() {
                view.onArticleResponeFail("Fail");
            }
        });
    }

    @Override
    public void onDeleteArticleById(int id) {

        interactor.deleteArticleById(id, new ArticleMVP.Interactor.DeleteInterractorCallback() {
            @Override
            public void onDeleteSuccess() {
                interactor.findAllArticles(1, new ArticleMVP.Interactor.InteractorCallBack() {
                    @Override
                    public void onSuccess(List<Article> articles) {
                        view.onArticleResponse(articles);
                    }
                    @Override
                    public void onFailed() {
                        view.onArticleResponeFail("Fail");
                    }
                });
            }

            @Override
            public void onDeleteFailed(Throwable t) {

            }
        });
    }


    @Override
    public void setView(ArticleMVP.View view) {
        this.view = view;
    }

    @Override
    public void onLoadMore(int page) {
        interactor.loadMorePage(page, new ArticleMVP.Interactor.LoadMorePageInteractorCallback() {
            @Override
            public void onLoadMoreSuccess(List<Article> list) {
                view.onReloadData(list);
            }

            @Override
            public void onLoadMoreFail(Throwable t) {

            }
        });
    }
}
