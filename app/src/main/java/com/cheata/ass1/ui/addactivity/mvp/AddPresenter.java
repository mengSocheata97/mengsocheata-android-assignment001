package com.cheata.ass1.ui.addactivity.mvp;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.cheata.ass1.data.api.entities.Article;
import com.cheata.ass1.data.api.response.ArticleResponse1;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class AddPresenter implements AddMVP.AddPresenter {

    private AddMVP.AddView addView;
    private AddMVP.AddInteractor interactor;

    public AddPresenter(AddMVP.AddView view)
    {
        this.addView = view;
        interactor = new AddInteractor();
    }

    @Override
    public void addArticle(Article article) {
        interactor.onAddArticle(article, new AddMVP.AddInteractor.AddInteractorCallback() {
            @Override
            public void onAddSuccess() {
                addView.onDestroyView();
            }
            @Override
            public void onAddFailed(Throwable t) {
                addView.onFailedView(t);
            }
        });
    }

    @Override
    public void getArticleById(int id) {
        interactor.onGetArticleById(id, new AddMVP.AddInteractor.GetArticleByIdInteractorCallback() {
            @Override
            public void onGetArticleByIdSuccess(ArticleResponse1 article) {
                if(article!=null){
                    addView.onGetSuccess(article.getData());
                }
            }

            @Override
            public void onGetArticleByIdFailed(Throwable t) {
                addView.onFailedView(t);
            }
        });
    }

    @Override
    public void addImageToServer(Context context, Uri img) {
        interactor.uploadImg(img, context, new AddMVP.AddInteractor.UploadImageInteractorCallback() {
            @Override
            public void onUploadSuccess(String path) {
                addView.onAddImageSuccess(path);

                Log.d(TAG, "onUploadSuccess: "+path);
            }
            @Override
            public void onUploadFailed(Throwable t) {
                addView.onFailedView(t);
            }
        });
    }

    @Override
    public void updateArticleById(int id, Article article) {
        interactor.onUpdateArticle(id, article, new AddMVP.AddInteractor.UpdateInteractorCallback() {
            @Override
            public void onUpdateSuccess() {
                addView.onDestroyView();
            }

            @Override
            public void onUpdateFailed(Throwable t) {
                addView.onFailedView(t);
            }
        });
    }
}
