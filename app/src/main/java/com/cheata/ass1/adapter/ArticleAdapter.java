package com.cheata.ass1.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.cheata.ass1.R;
import com.cheata.ass1.data.api.entities.Article;
import com.cheata.ass1.ui.addactivity.AddArticle;
import com.cheata.ass1.ui.main.mvp.ArticleMVP;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyViewHolder> {

    private List<Article> articles;
    private Context mContext;
    private ArticleMVP.Presenter presenter;

    public ArticleAdapter(List<Article> articles, Context mContext, ArticleMVP.Presenter presenter) {
        this.articles = articles;
        this.mContext = mContext;
        this.presenter = presenter;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.costom_card,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        final Article article = articles.get(position);
        Glide.with(mContext).load(articles.get(position).getImage())
                .placeholder(R.drawable.blue_dafault)
                .into(holder.imgImage);

        holder.textDes.setText(articles.get(position).getDescription());
        holder.textTilte.setText(articles.get(position).getTitle());

//        to convert time
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
        holder.textDate.setText(simpleDateFormat.format(Long.parseLong(articles.get(position).getCreatedDate())));

        holder.textmenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               PopupMenu popupMenu = new PopupMenu(v.getContext(),holder.textmenu);
                popupMenu.inflate(R.menu.option_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                       switch (item.getItemId()){
                           case R.id.menu_edit:
                               Intent intent = new Intent(mContext, AddArticle.class);
                               intent.putExtra("articleID",article.getId());
                               intent.putExtra("editView",1);
                               mContext.startActivity(intent);
                               break;
                           case R.id.menu_delete:
                               delete(articles.get(position).getId());
                               break;
                       }
                       return false;
                    }
                });
                popupMenu.show();
            }
        });
    }
    private void delete(final int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(false)
                .setTitle("Delete")
                .setMessage("Are you sure to delete?");
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.onDeleteArticleById(id);
            }
        })
        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView textTilte, textDes, textDate, textmenu;
        ImageView imgImage;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgImage = itemView.findViewById(R.id.img_card);
            textDate = itemView.findViewById(R.id.text_date);
            textTilte = itemView.findViewById(R.id.text_title);
            textDes = itemView.findViewById(R.id.text_desc);
            textmenu = itemView.findViewById(R.id.mn_menu);

        }
    }


}
